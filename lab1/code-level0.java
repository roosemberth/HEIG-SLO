// items = ['Rock', 'Paper', 'Scissors', 'Lizard', 'Spock']

class Foo {
  public String solve(String userValue, String srvValue) {
      switch (userValue) {
          case "Lizard":
              if (srvValue.equals("Spock") || srvValue.equals("Paper"))
                  return "Win";
              else if (srvValue.equals("Lizard"))
                  return "Draw";
              break;
          case "Spock":
              if (srvValue.equals("Scissors") || srvValue.equals("Rock"))
                  return "Win";
              else if (srvValue.equals("Spock"))
                  return "Draw";
              break;
          case "Paper":
              if (srvValue.equals("Spock") || srvValue.equals("Rock"))
                  return "Win";
              else if (srvValue.equals("Paper"))
                  return "Draw";
          case "Scissors":
              if (srvValue.equals("Lizard") || srvValue.equals("Paper"))
                  return "Win";
              else if (srvValue.equals("Scissors"))
                  return "Draw";
              break;
          case "Rock":
              if (srvValue.equals("Lizard") || srvValue.equals("Scissors"))
                  return "Win";
              else if (srvValue.equals("Rock"))
                  return "Draw";
              break;
          default:
              return "Lose";
      }
      return "Lose";
  }
}
