---
title: Lab1 Lizord
author: Roosembert Palacios
date: Tue Mar  9 05:27:48 PM CET 2021
---
## Niveau 0

Port 9993

> Trouver le flag en gagnant 10 fois de suite

```
$ (while true; do (echo Paper; sleep 0.1); done) | nc -vv 10.192.72.221 9993
Connection to 10.192.72.221 9993 port [tcp/palace-2] succeeded!
Welcome to Rock-Paper-Scissors-Lizard-Spock Game v0!
Choose :
Computer chose Lizard. Win
[...]
Choose :
Computer chose Lizard. Win
Congratulations, you won 10 times in a row !

Wow, you're clearly the best !
Take this as a present.

Here is your flag: WW91IHRha2UgdGhlIHJlZCBwa
```

### Question 3.1

> Quelle erreur peut-on voir dans ce code?

Missing `break` statement at line 22. [CWE-484][CWE484]: Omitted Break Statement
in Switch; The program omits a break statement within a switch or similar
construct, causing code associated with multiple conditions to execute.
This can cause problems when the programmer only intended to execute code
associated with one condition.

[CWE484]: https://cwe.mitre.org/data/definitions/484.html

### Question 3.2

> Comment peut-on exploiter ces erreurs pour jamais perdre

Il suffit de répondre 'Paper' à chaque question.

### Question 3.3

> Comment pourrait-on corriger ce code

Add missing `break` statement.

The cognitive complexity of the code makes this class of errors highly likely.
Use a different DSL to express such logic, such as a lookup table. Functionnal
programming helps avoid this kind of erros.

## Niveau 1

Port 9991

> Trouver le flag en gagnant 10 fois de suite

```
[x] Opening connection to 10.192.72.221 on port 9991
[x] Opening connection to 10.192.72.221 on port 9991: Trying 10.192.72.221
[+] Opening connection to 10.192.72.221 on port 9991: Done
[*] Welcome to Rock-Paper-Scissors-Lizard-Spock Game v1!
[*] Rock crushes Scissors
[*] Scissors cut Paper
[*] Rock crushes Lizard
[*] Scissors cut Paper
[*] Spock melts Scissors
[*] Lizard poisons Spock
[*] Paper covers Rock
[*] Scissors cut Paper
[*] Scissors decapitate Lizard
[*] Paper covers Rock
[x] Receiving all data
[x] Receiving all data: 103B
[+] Receiving all data: Done (103B)
[*] Closed connection to 10.192.72.221 port 9991
[+]
    Wow, you're clearly the best !
    Take this as a present.

    Here is your flag: WxsLCB5b3Ugc3RheSBpbiBXb2

```

### Question 3.1

> Quelle est la faille présente dans cette application

Après avoir joué un peu avec on dirait que le prochain choix du serveur est
déterministe.
Il s'agit donc de la [CWE-330][CWE330]: _Use of Insufficiently Random Values_;
The software uses insufficiently random numbers or values in a security context
that depends on unpredictable numbers.

[CWE330]: https://cwe.mitre.org/data/definitions/330.html

### Question 4.2

> Comment peut-on l'exploiter pour ne jamais perdre

On peut construire une séquence qui bats toujours le serveur.
Par exemple, pour gagner 10 fois on peut utiliser la séquence suivante:
Rock, Scissors, Rock, Scissors, Spock, Lizard, Paper, Scissors, Scissors, Paper.


### Question 4.3

> Comment pourrait-on corriger cette erreur

Mieux initialiser le générateur aléatoire de l'application.

## Niveau 2

Port 9996

### Manipulation 5.1

> Trouver la zone administrateur de l’application.

Intuition calls for typing something like 'admin' and as luck would have, we're
greet with a password message soon after

```
[+] Opening connection to 10.192.72.221 on port 9996: Done
[*] Welcome to Rock-Paper-Scissors-Lizard-Spock Game v2!
[*] Running command to enter admin zone...
[*] === You are entering the admin zone ===
[*] Wrong password length, trying with !!
[*] Wrong password length, trying with !!!
[*] Wrong password length, trying with !!!!
[*] Wrong password length, trying with !!!!!
[*] Wrong password length, trying with !!!!!!
[*] Found character at position 0: 3!!!!!
[*] Found character at position 1: 30!!!!
[*] Found character at position 2: 308!!!
[*] Found character at position 3: 3082!!
[*] Found character at position 4: 30821!
[*] Unexpected sequence follows, terminating with state: pass: 308217, idx: 5
[*] Welcome! Here is your flag: 5kZXJsYW5kLCBhbmQgSSBzaG9
```

### Question 5.1

> Quelle est la faille de cette fonctionnalité ?

[CWE-200][CWE200]: Exposure of Sensitive Information to an Unauthorized Actor;
The product exposes sensitive information to an actor that is not explicitly
authorized to have access to that information.

[CWE200]: https://cwe.mitre.org/data/definitions/200.html

### Question 5.2

> Comment peut-on l’exploiter ?

We can use the information exposed by the server for greatly improve our guess
of the admin password.

### Manipulation 5.2

> Trouver le flag en entrant le bon mot de passe.

Password: `308216`
Flag: `5kZXJsYW5kLCBhbmQgSSBzaG9`

### Question 5.3

> Comment pourrait-on corriger cette erreur ?

Don't give to many details about the password to the user.
Do not acknowledge the existence of a given username.
Do use passwords with higher entropy.
More importantly, do not use such easy users such as 'admin'.

## Niveau 3

Port 9999

### Question 6.1

> Quelle est la faille présente dans cette application

Since we have a custom client, that means there could be smarted code running in
the client. By looking at the TCP conversation, one can see the following
pattern:

```
$ tshark -q --color -i any -f 'port 9999' -x
...
0000  00 00 ff fe 00 00 00 00 00 00 00 00 00 00 08 00   ................
0010  45 00 00 51 b9 03 40 00 3c 06 10 8f 0a c0 48 dd   E..Q..@.<.....H.
0020  0a c1 16 b7 27 0f 9f 80 4b a9 b3 8a 5c 1a ff f2   ....'...K...\...
0030  80 18 01 fd a6 b4 00 00 01 01 08 0a 49 c0 9b 3e   ............I..>
0040  f1 c9 c3 42 44 45 42 55 47 3b 53 70 6f 63 6b 0a   ...BDEBUG;Spock.
0050  41 43 54 49 4f 4e 3b 43 68 6f 6f 73 65 20 3a 20   ACTION;Choose :
0060  0a                                                .
```

We can identify a 'debug' command, and a bit of experimentation concludes this
corresponds to the next answer from the server.

This corresponds to [CWE-532][CWE532]: Insertion of Sensitive Information into
Log File; Information written to log files can be of a sensitive nature and give
valuable guidance to an attacker or expose sensitive user information.

CWE532: https://cwe.mitre.org/data/definitions/532.html

### Question 6.2

> Comment peut-on l'exploiter pour ne jamais perdre

We can look at the network messages and deduce the next server move and reply
an anwer that beats it.

### Manipulation 6.1

> Trouver le flag en gagnant 5 fois de suite.

```
Congratulations, you won 5 times in a row !
Wow, you're clearly the best ! Take this as a present.
Here is your flag: 3IHlvdSBob3cgZGVlcCB0aGUg
```

### Question 6.3

> Comment pourrait-on corriger cette erreur

Don't send unnecessary data to the client. More importantly, information flows
containing sensitive details MUST be separate from these containing user data.

## Niveau bonus

### Manipulation 7.1

> Trouver le numéro de port.

Start by scanning the host ports (limited to the 9xxx range because of
intuition).

```
$ sudo nmap -n -T5 -sS 10.192.72.221 -p9000-10000
Starting Nmap 7.80 ( https://nmap.org ) at 2021-03-10 09:55 CET
Nmap scan report for 10.192.72.221
Host is up (0.021s latency).
Not shown: 996 closed ports
PORT     STATE SERVICE
9991/tcp open  issa
9993/tcp open  palace-2
9996/tcp open  palace-5
9998/tcp open  distinct32
9999/tcp open  abyss
```

The only port we haven't played with so far is 9998.

```
$ nc -vv 10.192.72.221 9998
Connection to 10.192.72.221 9998 port [tcp/distinct32] succeeded!
Welcome to Rock-Paper-Scissors-Lizard-Spock Game v4!
Choose :
```

Looks like the next target.

### Question 7.1

> Quelle est la faille présente dans cette application

After playing for while, it would seem like the server's next pick is always the
last from the user.

This is an instance of [CWE-330][CWE330]: _Use of Insufficiently Random Values_;
The software uses insufficiently random numbers or values in a security context
that depends on unpredictable numbers.

### Question 7.2

> Comment peut-on l'exploiter pour ne jamais perdre

Lucky guess and succesive attempts pick a choice that beats your previous reply.

### Manipulation 7.2

> Trouver le flag en gagnant 10 fois de suite.

```
Congratulations, you won 10 times in a row !

Wow, you're clearly the best !
Take this as a present.

Here is your flag: cmFiYml0IGhvbGUgZ29lcy4=
```

### Manipulation 7.3

> Bonus+ : Trouver le message caché dans les flags.

```
echo cmFiYml0IGhvbGUgZ29lcy4= | base64 -d
rabbit hole goes.
```
