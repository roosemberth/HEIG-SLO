# Exercise 2

files:

- functions1
- functions2
- jumpMania

Exploration:

- The files have an executable permission.
- `ldd` reveals none of them are a dynamic executable.
- `file` reveals they're all ELF 32-bit executable requesting /lib/ld-linux.so.2 interpreter.
- Launch Ghidra, open functions1

### functions1 on Ghidra
