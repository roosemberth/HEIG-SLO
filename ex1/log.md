# Reverse1

- Load the binary into Ghidra
- Run analysis
- Look for an entry point in the symbol tree => main
- Look at the decompiled code
- I noticed there's a conditional giving me the flag.
  ```C
  printf("Enter a value: ");
  __isoc99_scanf(&DAT_080485b0,local_14);
  if (local_14[0] == 0x2a) {
    puts("Congratulations! You found the secret value!");
  }
  else {
    puts("Wrong value.");
  }
  ```

- When running the program, it asks for an input.
- Convert `0x2a` to decimal => _42_.
- Run the program, enter 42.
  ```
  tmp-chrootenv:roosemberth@Mimir:~/ws/3-Orgs/HEIG/2021-SLO-B/labs/ex1$ ./reverse1
  Enter a value: 22
  Wrong value.
  tmp-chrootenv:roosemberth@Mimir:~/ws/3-Orgs/HEIG/2021-SLO-B/labs/ex1$ ./reverse1
  Enter a value: 42
  Congratulations! You found the secret value!
  ```
- Profit

# Reverse2

- Load the binary into Ghidra
- Run analysis
- Look for an entry point in the symbol tree => main
- Look at the decompiled code
- Similar to the previous exercise:
  ```C
  if ((local_11[0] < 's') || ('s' < local_11[0])) {
    puts("Wrong value.");
  }
  ```

- Try with character 's'
  ```
  tmp-chrootenv:roosemberth@Mimir:~/ws/3-Orgs/HEIG/2021-SLO-B/labs/ex1$ ./reverse2
  Enter a char: s
  Congratulations! You found the secret value!
  ```

- Profit
