---
title: "Lab 2 SLO: Ransomware"
lang: fr
author:
  - Roosembert Palacios
  - Florian Gazzetta
geometry: margin=2cm
---

## Rappels de C

### Manipulation 3.1

```c
void encrypt(FILE *fp) {
  const int pad = 0xff;
  int ch;
  while ((ch = fgetc(fp)) != EOF) {
    fseek(fp, -1, SEEK_CUR); // Rewind to the byte we just read.
    fputc(pad ^ ch, fp);
  }
}
```

### Question 3.1

> Comment pouvez-vous déchiffrer ce fichier ?

- Chaque byte est chiffré indépendament à l'aide de l'opération XOR.
- L'inverse de l'opération XOR est aussi l'opération XOR.

On peut donc appliquer exactement la même opération de chiffrement pour
déchiffrer le fichier; ce qui équivaut à chiffrer à nouveau le document chiffré.

### Manipulation 3.2

> Nous avons testé que exécuter le binaire deux fois sur le même fichier rend le
> fichier d'origine.

![Appel à fseek dans Ghidra](img/3.2.1-Ghidra-fseek-call.png)

Nous pouvons observer que les arguments `1`, `-1` et `__block` (le pointeur au
fichier) sont placés dans la stack avant d'appeler `fseek`.
Pour rappel, la fonction `fseek` a pour signature `int fseek(FILE *, long ,
int);`.
Dans notre cas, on utilise `fseek(fp, -1, SEEK_CUR);`, et le code assembleur
révélé par Ghidra nous évoque la [_calling convention_ X86 **cdecl**][cdecl].
Ceci implique que la constante `SEEK_CUR` équivaut à `1`.

[cdecl]: https://en.wikipedia.org/wiki/X86_calling_conventions

\pagebreak

## 2ème partie : structure du code

### Manipulation 4.1

![Fonction _encrypt dir_ analysée dans Ghidra](img/4.1.1-Ghidra-encrypt_dir-solved.png)

Sur la capture d'écran on peut voir que le chiffrement de fichiers commence avec
la comparaison du nom de fichier pour sélectionner le chiffrement à utiliser.

### Question 4.1

> Que fait la partie du code C avant le chiffrement ?

Elle parcours tous les éléments du répertoire, appelle récursivement
`encrypt_dir` avec les sous-répertoires et choisi une fonction de chiffrement à
utiliser s'il s'agit d'un fichier régulier.
Il évite les dossiers "." et "..", ainsi qu'un fichier dont le nom lui est passé en paramètre.
Si on étudie la méthode main, on découvre que le nom de fichier qui lui est passé est le nom
du ransomware avec son chemin complet.

### Manipulation 4.2

(Voir capture d'écran de la manipulation 4.1).

### Question 4.2

> Que fait le code C du chiffrement qui se trouve dans la fonction
encrypt_dir?

La fonction regarde les deux derniers bits du nombre de caractères dans le nom du fichier.
Si ces bits sont 00, le programme utilise la première méthode d'encription (encrypt0). Si ces bits
sont 01, le programme utilise la deuxième méthode d'encription (encrypt1). Dans les autres cas, le
programme utilise la troisième méthode d'encription (encrypt2).
On peut en déduire que le fichier confidential a été crypté avec la méthode encrypt0.
On peut en déduire que le fichier passwords a été crypté avec la méthode encrypt1.
On peut en déduire que le fichier salary a été crypté avec la méthode encrypt2.

### Question 4.3

> Identifiez les parties de gestion de la pile du code assembleur correspondant
> à la partie de chiffrement.

![Gestion de la pile avant l'appel à la fonction de chiffrement de fichier](
img/4.3.1-Ghidra-call-stack-encrypt0.png)

Les appels aux fonctions de chiffrement sont très simples et ne requièrent q'un
seul argument.

Le code de chaque fonction de chiffrement est le même dans le cas de `encrypt1`
et `encrypt2`.

> Expliquez les instructions qui implémentent la logique du chiffrement (dans la
> fonction `encrypt_dir`).

```asm
0804967f e8 2c fa        CALL       fopen         ; Open the file
         ff ff
08049684 83 c4 10        ADD        ESP,0x10      ; Remove call arg
                                                  ; Save pointer to inode
08049687 89 45 ec        MOV        dword ptr [EBP + pInode], EAX
0804968a 8b 45 f0        MOV        l1,dword ptr [EBP + pInode]
0804968d 83 c0 0b        ADD        l1,0xb        ; Seek to inode name
08049690 83 ec 0c        SUB        ESP,0xc       ; Make some space
08049693 50              PUSH       l1            ; Addr of inode name
08049694 e8 f7 f9        CALL       strlen        ; Find length of filename
         ff ff
08049699 83 c4 10        ADD        ESP,0x10      ; Remove call arg
0804969c 0f be c0        MOVSX      eax,al        ; Cast LSB EAX to int
0804969f 89 45 e8        MOV        dword ptr [EBP + l2],eax
                                                  ; Take last two bits
080496a2 83 65 e8 03     AND        dword ptr [EBP + l2],0x3
                                                  ; length ends with 0b00?
080496a6 83 7d e8 00     CMP        dword ptr [EBP + l2],0x0
080496aa 75 10           JNZ        LAB_080496bc  ; Else jump to WHEN1
080496ac 83 ec 0c        SUB        ESP,0xc       ; Prepare stack, push pInode
080496af ff 75 ec        PUSH       dword ptr [EBP + pInode]
080496b2 e8 8f fb        CALL       encrypt0
         ff ff
080496b7 83 c4 10        ADD        ESP,0x10      ; Remove call arg
080496ba eb 24           JMP        LAB_080496e0  ; Goto END.
                     LAB_080496bc                 ; WHEN1: length ends with 0b01?
080496bc 83 7d e8 01     CMP        dword ptr [EBP + l2],0x1
080496c0 75 10           JNZ        LAB_080496d2  ; Else jump to WHEN2
080496c2 83 ec 0c        SUB        ESP,0xc       ; Prepare stack, push pInode
080496c5 ff 75 ec        PUSH       dword ptr [EBP + pInode]
080496c8 e8 cf fb        CALL       encrypt1
         ff ff
080496cd 83 c4 10        ADD        ESP,0x10      ; Remove call arg
080496d0 eb 0e           JMP        LAB_080496e0  ; Goto END.
                     LAB_080496d2                 ; WHEN2: length ends with 0b1X
080496d2 83 ec 0c        SUB        ESP,0xc       ; Prepare stack, push pInode
080496d5 ff 75 ec        PUSH       dword ptr [EBP + pInode]
080496d8 e8 bb fd        CALL       encrypt2
         ff ff
080496dd 83 c4 10        ADD        ESP,0x10      ; Remove call arg
                     LAB_080496e0                 ; END
080496e0 83 ec 0c        SUB        ESP,0xc       ; Rest of code...
```

\pagebreak

## 3ème partie : fonction `encrypt0`

### Manipulation 5.1

![Fonction _encrypt0_ analysée dans Ghidra](img/5.1.1-Ghidra-encrypt0-solved.png)

### Question 5.1

> Que fait le code C affiché par Ghidra ?

Le code C ajoute 186 à chaque caractère du fichier, de la même manière qu'un
chiffre de César.

### Manipulation 5.2

```C
void encrypt0(FILE *pFile) {
  int c;
  while (true) {
    c = fgetc(pFile);             // Read a byte from file. Seeks one byte forward.
    if (c == -1) break;           // Break from loop if read EOF.
    fseek(pFile,-1,1);            // Seek back one byte.
    fputc((char)c + 0xba,pFile);  // Write the ciphered character to the file.
                                  // Seeks one byte forward.
  }
  return;
}
```

### Question 5.2

> Identifiez les parties de gestion de la pile du code assembleur correspondant
> à la partie de chiffrement.

```asm
08049249 83 ec 18        SUB        ESP,0x18      ; Make some space.
0804924c eb 33           JMP        LAB_08049281  ; Goto READ.
                     LAB_0804924e                 ; WORK
0804924e 83 ec 04        SUB        ESP,0x4       ; Make some space.
08049251 6a 01           PUSH       0x1           ; Push SEEK_CUR.
08049253 6a ff           PUSH       -0x1          ; Push -1 and pFile.
08049255 ff 75 08        PUSH       dword ptr [EBP + pFile]
08049258 e8 13 fe        CALL       fseek         ; Seek pFile one byte backwards.
         ff ff
0804925d 83 c4 10        ADD        ESP,0x10      ; Remove call args
                                                  ; Load the char into EAX
08049260 8b 45 f4        MOV        EAX,dword ptr [EBP + l0]
08049263 0f be c0        MOVSX      EAX,AL        ; Take the lower byte of EAX
08049266 89 45 f4        MOV        dword ptr [EBP + l0],EAX
                                                  ; Add 186 to the byte
08049269 81 45 f4        ADD        dword ptr [EBP + l0],0xba
         ba 00 00 00
08049270 83 ec 08        SUB        ESP,0x8       ; Make some space, push pFile
08049273 ff 75 08        PUSH       dword ptr [EBP + pFile]
                                                  ; Push the new value
08049276 ff 75 f4        PUSH       dword ptr [EBP + l0]
08049279 e8 82 fe        CALL       fputc         ; Write to file
         ff ff
0804927e 83 c4 10        ADD        ESP,0x10      ; Remove call args
                     LAB_08049281                 ; READ
08049281 83 ec 0c        SUB        ESP,0xc       ; Make some space + push pFile
08049284 ff 75 08        PUSH       dword ptr [EBP + pFile]
08049287 e8 54 fe        CALL       fgetc         ; Get a byte
         ff ff
0804928c 83 c4 10        ADD        ESP,0x10      ; Remove call args
                                                  ; Store the byte in l0
0804928f 89 45 f4        MOV        dword ptr [EBP + l0],EAX
08049292 83 7d f4 ff     CMP        dword ptr [EBP + l0],-0x1
08049296 75 b6           JNZ        LAB_0804924e  ; If EAX != EOF => Goto WORK
08049298 90              NOP                      ; Padding for some arbitrary
08049299 90              NOP                      ; binary reason.
0804929a c9              LEAVE
0804929b c3              RET
```

### Manipulation 5.3

L'on peut modifier le code de la _manipulation 3.1_ comme suit:

```C
void encrypt(FILE *fp) {
  const int pad = -186;
  int ch;
  while ((ch = fgetc(fp)) != EOF) {
    fseek(fp, -1, SEEK_CUR); // Rewind to the byte we just read.
    fputc(pad + ch, fp);
  }
}
```

On peut ensuite vérifier le résultat:

```
$ ./modify home/confidential
$ cat home/confidential
The SLO 2021 final exam will contain a reverse engineer exercise.
```

\pagebreak

## 4ème partie : fonction `encrypt1`

### Manipulation 6.1

![Fonction _encrypt1_ analysée dans Ghidra](img/6.1.1-Ghidra-encrypt1-solved.png)

### Question 6.1

> Que fait le code C affiché par Ghidra ?

Le code C effectue un décalage binaire rotatif de 2 vers la droite ainsi qu'un XOR
avec le caractère précédent.

### Manipulation 6.2

```C
void encrypt1(FILE *pFile) {
  unsigned char precedChar = 0;

  while (1) {
    int ch = fgetc(pFile); // Read a byte from file. Seeks one byte forward.
    if (ch == -1)          // EOF = -1. If reached EOF stop processing.
        break;             // Break from loop if read EOF.
    fseek(pFile, -1, 1);   // Seek back one byte.
    unsigned char read = (char)ch;
    unsigned char rotated = (read >> 2) | (read << 6);
    unsigned char xored = rotated ^ precedChar;
    precedChar = read; // Write the ciphered character to the file.
    fputc(xored, pFile);
  }
  return;
}
```

### Question 6.2

> Identifiez les parties de gestion de la pile du code assembleur correspondant
> à la partie de chiffrement.

```asm
0804929f 83 ec 18        SUB        ESP,0x18      ; Make some space.
080492a2 c6 45 f7 00     MOV        byte ptr [EBP + precedChar],0x0
                                                  ; Store 0 in precedChar
080492a6 eb 57           JMP        LAB_080492ff  ; Goto READ
                    LAB_080492a8                  ; WORK
080492a8 83 ec 04        SUB        ESP,0x4       ; Make some space.
080492ab 6a 01           PUSH       0x1           ; Push SEEK_CUR.
080492ad 6a ff           PUSH       -0x1          ; Push -1 and pFile.
080492af ff 75 08        PUSH       dword ptr [EBP + pFile]
080492b2 e8 b9 fd        CALL       fseek         ; Seek pFile one byte backwards.
         ff ff
080492b7 83 c4 10        ADD        ESP,0x10      ; Remove call args
                                                  ; Load the char into EAX
080492ba 8b 45 f0        MOV        EAX,dword ptr [EBP + readByte]
080492bd 0f b6 c0        MOVZX      EAX,AL        ; Take the lower byte of EAX
                                                  ; Save the char in frame.
080492c0 88 45 ee        MOV        byte ptr [EBP + read],AL
                                                  ; Store constant value
080492c3 c7 45 e8        MOV        dword ptr [EBP + v2],0x2
         02 00 00 00
                                                  ; Load constant (2) to EDX
080492ca 8b 55 e8        MOV        EDX,dword ptr [EBP + v2]
                                                  ; Load char to EAX
080492cd 0f b6 45 ee     MOVZX      EAX,byte ptr [EBP + read]
080492d1 89 d1           MOV        ECX,EDX       ; Load constant (2) to ECX
080492d3 d2 c8           ROR        AL,CL         ; Right rotation of 2 bits of AL
                                                  ; Store rotated char
080492d5 88 45 ee        MOV        byte ptr [EBP + read],AL
                                                  ; Load char in EAX
080492d8 0f b6 45 ee     MOVZX      EAX,byte ptr [EBP + read]
                                                  ; Store char
080492dc 88 45 ef        MOV        byte ptr [EBP + rotated],AL
                                                  ; Load precedChar in EAX
080492df 0f b6 45 f7     MOVZX      EAX,byte ptr [EBP + precedChar]
                                                  ; XOR rotated char with precedChar
080492e3 30 45 ef        XOR        byte ptr [EBP + xored],AL
                                                  ; load read byte into EAX
080492e6 8b 45 f0        MOV        EAX,dword ptr [EBP + readByte]
                                                  ; Store read byte into precedChar
080492e9 88 45 f7        MOV        byte ptr [EBP + precedChar],AL
                                                  ; load XOR'd char into EAX
080492ec 0f b6 45 ef     MOVZX      EAX,byte ptr [EBP + xored]
080492f0 83 ec 08        SUB        ESP,0x8       ; Make some space, push pFile
080492f3 ff 75 08        PUSH       dword ptr [EBP + pFile]
080492f6 50              PUSH       EAX           ; Push the new value
080492f7 e8 04 fe        CALL       fputc         ; Write to file
         ff ff
080492fc 83 c4 10        ADD        ESP,0x10      ; Remove call args
                     LAB_080492ff                 ; READ
080492ff 83 ec 0c        SUB        ESP,0xc       ; Make some space + push pFile
08049302 ff 75 08        PUSH       dword ptr [EBP + pFile]
08049305 e8 d6 fd        CALL       fgetc         ; Get a byte
         ff ff
0804930a 83 c4 10        ADD        ESP,0x10      ; Remove call args
                                                  ; Store the byte in currentChar
0804930d 89 45 f0        MOV        dword ptr [EBP + readByte],currentChar
08049310 83 7d f0 ff     CMP        dword ptr [EBP + readByte],-0x1
08049314 75 92           JNZ        LAB_080492a8  ; If currentChar != EOF => Goto WORK
08049316 90              NOP                      ; Padding for some arbitrary
08049317 90              NOP                      ; binary reason.
08049318 c9              LEAVE
08049319 c3              RET
```

### Question 6.3

Dans ce code, on voit deux instructions que l'on avait pas vu en cours :
MOVZX : Déplace un registre vers un registre de taille supérieure
et remplis l'espace vide de zéros
ROR : Effectue une rotation a droite d'un certain de nombre de bits

En plus de ces instructions, dans la fonction encrypt0, on voit aussi
l'instruction MOVSX qui effectue la même opération que MOVZX sauf qu'elle
remplis l'espace vide avec des copies du bit de poids fort.

### Manipulation 6.3

L'on peut modifier le code de la _manipulation 3.1_ comme suit:

```C
void encrypt(FILE *fp) {
  char predClear = 0;
  int ch;
  while ((ch = fgetc(fp)) != EOF){
    fseek(fp,-1,SEEK_CUR);
    unsigned char xored = (char)ch;
    unsigned char rotated = xored ^ predClear;
    unsigned char plain = rotated << 2 | rotated >> 6;
    fputc(plain, fp);
    predClear = plain;
  }
}
```

À l'aide de ce code on peut déchiffrer le fichier `./home/passwords` :

```
maps.google.com: 123456
webmail.heig-vd.ch: qwertz
cryptobin.co/57453857: ZX0wrnF6KHQSoQkd
test.org: password
```

#### Digression sur les données déchiffrés

Si bien dans le métier de sécurité informatique, d'un point de vue étique on
devrait se contenter de déchiffrer les fichiers seulement sans prendre
connaissance de son contenu; pourvu qu'il s'agit d'un exercice académique, on
peut envisager de regarder d'un petit peu plus près les donnés obtenus.

En particulier, dans ce fichier on peut retrouver l'URL
<http://cryptobin.co/57453857>.
À l'aide de ce lien on peut retrouver le message :

```
eyJpdiI6Ilh4Q3FUVXIvMWJFWVFrbjRtU01hSGc9PSIsInYiOjEsIml0ZXIiOjEwMDAwLCJrcyI6MjU2
LCJ0cyI6NjQsIm1vZGUiOiJjY20iLCJhZGF0YSI6IiIsImNpcGhlciI6ImFlcyIsInNhbHQiOiJpYkND
d0UyTXpsRT0iLCJjdCI6IjVpVCtVa05uSXhrNGRhRlMzRmdQTnk1b1JIeHpaQnZXdzdqNHZ6b0RpMWJk
TmdrcDVOYlBMZzFsVVRUNEdRUkcwd3F2In0=
```

(retours à la ligne insérés pour la mise en forme.)

Le début et le caractère '=' à la fin du fichier nous évoquent un message encodé
en base64, en effet on peut le décoder à l'aide de la commande `base64 -d` pour
retrouver un texte JSON (modifié pour la mise en forme):

```
{
  "iv": "XxCqTUr/1bEYQkn4mSMaHg==",
  "v": 1,
  "iter": 10000,
  "ks": 256,
  "ts": 64,
  "mode": "ccm",
  "adata": "",
  "cipher": "aes",
  "salt": "ibCCwE2MzlE=",
  "ct": "5iT+UkNnIxk4daFS3FgPNy5oRHxzZBvWw7j4vzoDi1bdNgkp5NbPLg1lUTT4GQRG0wqv"
}
```

On peut utiliser le mot de passe dans le fichier `home/passwords` pour
déchiffrer le contenu ce qui nous révèle le lien suivant:
<https://www.youtube.com/watch?v=oHg5SJYRHA0>.

\pagebreak

## 5ème partie : fonction `encrypt2`

### Manipulation 7.1

![Fonction _encrypt2_ analysée dans Ghidra](img/7.1.1-Ghidra-encrypt2-solved.png)

### Question 7.1

> Que fait le code C affiché par Ghidra ?

Il commence par generer 16 clef de chiffrement a l'aide de la fonction generateKey
puis il commence par faire un boucle qui sert a faire deux opérations : 
un xor entre tout les caractères du fichier, et pour chaque caractère, il remplace
le caractère par un xor entre ce caractère et la clef dont le numéro est égale
a l'indice du caractère modulo 15.
Ensuite, il effectue une seconde boucle qui remplace tout les caractère par un
xor entre ce caractère et le xor global du fichier qui a été calculé a l'étape
précédente.

### Manipulation 7.2

```C
void encrypt2(FILE *fp) {
  byte key[16];
  generateKey(key, 0x10);

  byte rollling_xor = 0;
  unsigned int i = 0;

  while (1) {
    int ch = fgetc(fp); // Write char seeks one byte forward
    if (ch == -1) // If EOF, exit loop.
      break;
    fseek(fp, -1, 1); // rewind one char
    byte read = (byte)ch;
    ch = (int)(char)read;
    rollling_xor = rollling_xor ^ read;
    ch = (int)(char)(read ^ key[i]);
    fputc(ch, fp); // Write char seeks one byte forward
    i = i + 1 & 0xf;
  }

  fseek(fp, 0, 0); // Seek back the file to the beginning

  while (1) {
    int ch = fgetc(fp); // Write char seeks one byte forward
    if (ch == -1) // If EOF, exit loop.
      break;
    fseek(fp, -1, 1); // rewind one char
    ch = (int)(char)((byte)ch ^ rollling_xor);
    fputc(ch, fp); // Write char seeks one byte forward
  }
  fputc((int)(char)rollling_xor, fp);
  return;
}
```

### Question 7.2

> Pourquoi n’est-il pas nécessaire, pour restaures les fichiers chiffrés avec
encrypt2, de faire le Reverse des fonctions appelées par cette fonction ?

La fonction encrypt2 utilise la fonction generateKey, mais il n'est pas nécessaire
de reverse cette fonction car elle ne fais pas d'appel système et les deux seuls
paramètres de cette fonction sont le tableau des clef a remplir et la taille de
ce tableau, qui sont deux paramètre constants donc il suffit d'executer une fois la
fonction dans un autre contexte pour pouvoir récupérer les valeurs grace au débugger
et les utiliser pour déchiffrer le fichier.

### Manipulation 7.3

On peut exécuter le binaire à l'aide de `GDB` et break juste après que la
fonction `generateKey` ait été appelé pour obtenir la clé :

![Frame dans GDB après avoir generé la clé, fichier _salary_ déchiffré
](img/7.3.1-GDB-find-encryption-key.png)

```C
#include <unistd.h>

void encrypt(FILE *fp) {
  fseek(fp, -1, SEEK_END);
  int ch = fgetc(fp);
  if (ch == EOF) // File is invalid.
    return;
  unsigned char pad = ch; // pad is in the last character

  fseek(fp, 0, SEEK_SET); // Seek back the file to the beginning
  while ((ch = fgetc(fp)) != EOF){
    fseek(fp,-1,SEEK_CUR);
    unsigned char xored = (char)ch;
    unsigned char plain = xored ^ pad;
    fputc(plain, fp);
  }

  // Extracted by gadgetting the ransomware
  unsigned char key[16] = {
    0xf0,0xaf,0x56,0x5c,0x02,0xbe,0x44,0x88,
    0x09,0x27,0xce,0x75,0x3d,0xcf,0x80,0xc8
  };
  unsigned int i = 0;

  fseek(fp, 0, SEEK_SET); // Seek back the file to the beginning
  while (1) {
    int ch = fgetc(fp); // Write char seeks one byte forward
    if (ch == -1) // If EOF, exit loop.
      break;
    fseek(fp, -1, 1); // rewind one char
    byte read = (byte)ch;
    ch = (int)(char)(read ^ key[i]);
    fputc(ch, fp); // Write char seeks one byte forward
    i = i + 1 & 0xf;
  }

  // Seek to the end and truncate file to remove stranded character left by the
  // encryption.
  fseek(fp, -1, SEEK_END);
  ftruncate(fileno(fp), ftell(fp));
}
```

À noter le header supplémentaire et l'utilisation de `ftruncate` pour rétirer
le byte supplémentaire ajouté en plus par la fonction de chiffrement.
Ceci n'est pas réfleté dans la capture d'écran.

\pagebreak

## 6ème partie : conclusion

### Manipulation 8.1

fichier confidential :
```
The SLO 2021 final exam will contain a reverse engineer exercise. 
```

fichier passwords :
```
maps.google.com: 123456
webmail.heig-vd.ch: qwertz
cryptobin.co/57453857: ZX0wrnF6KHQSoQkd
test.org: password
```

fichier salary :
```
The HEIG-VD decided to give you a salary of 1'000'000CHF per year. 
This salary will be paid by tax money. 
In exchange, you will be asked to stay a student for the rest of your life. 
```
