#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void encrypt(FILE *fp) {
  const int pad = 0xff;
  int ch;
  while ((ch = fgetc(fp)) != EOF) {
    fseek(fp, -1, SEEK_CUR); // Rewind to the byte we just read.
    fputc(pad ^ ch, fp);
  }
}

int main(int argc, char **argv) {

  FILE *fp;

  if (argc != 2) {
    fprintf(stderr, "Usage: %s <file to encrypt>\n", argv[0]);
    return -1;
  }

  /* Opening the file */
  fp = fopen(argv[1], "r+");

  if (fp == NULL) {
    fprintf(stderr, "Failed to open file %s\n", argv[1]);
    return -1;
  }

  encrypt(fp);

  /* Close the file */
  fclose(fp);
  return 0;
}
