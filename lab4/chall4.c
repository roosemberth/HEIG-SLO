
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define STR_(x) #x
#define STR(x) STR_(x)

void fail() {
    printf("You failed \n");
    exit(0);
}

void win() {
    printf("WIN\n");
    system("cat flag");
}

int main(int argc, char **argv) {
    char *login_addr;
    void (*function)();
    char login[BUFFER_SIZE];
    char pass[BUFFER_SIZE];
    login_addr = login;
    
    setvbuf(stdout, NULL, _IONBF, 0);

    function = fail;
    
    printf("Login : ");
    scanf("%" STR(BUFFER_SIZE) "s", login);
    
    printf(login_addr);
    printf("\n");
    printf("Password : ");
    
    scanf("%" STR(BUFFER_SIZE) "s", pass);
    printf(pass);
    function();
    return 0;
}
