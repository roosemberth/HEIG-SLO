---
title: "Lab 4 SLO: Exploitation"
lang: fr
author:
  - Roosembert Palacios
  - Florian Gazzetta
geometry: margin=2cm
---

## Buffer overflows - niveau 1

### Question 3.1

> Quelle fonction fait référence à la fonction `win` et comment s'y prend-elle ?
> Que fait cette fonction avec la référence à `win` ?

La fonction `login` retoune une fonction. Cette fonction peut être `fail` ou
`win`.

> Dans quelles conditions, la fonction `win` sera-t-elle exécutée ?

Si la fonction `login` retourne la fonction `win`, elle sera executée par la
fonction `main`.

> Au final, quelle fonction fait vraiment l'appel à `win` ?

La fonction `main`.

### Question 3.2

> Quelle vulnérabilite pourrait nous permettre d'assurer l'appel à la fonction
> `win` ?

Un buffer overflow.
Lors de l'analyse du binaire avec Ghidra, l'on peut trouver une utilisation de
la fonction à risque `scanf` qui copie une chaîne de caractères contrôlé par
l'utilisateur (dans ce cas, `stdin`) sur un buffer de taille fixe (320 bytes).

### Question 3.3

> Dessiner la stack frame de la fonction qui fait référence à `win`.

La frame de la fonction `login` devrait, d'après le code décompilé devrait
ressembler à ceci.

```text
-------------------------------------------
| $ebp+4     | Return address (4)         |
-------------------------------------------
| $ebp       | Saved frame pointer (4)    |
-------------------------------------------
| $ebp-4     | cmp_result (4)             |
-------------------------------------------
| $ebp-8     | passwd_buf ptr (4)         |
-------------------------------------------
| $ebp-12    | rand_buf ptr (4)           |
-------------------------------------------
| $ebp-16    | callback function ptr (4)  |
-------------------------------------------
| $ebp-0x150 | rand_buf (320)             |
-------------------------------------------
| $ebp-0x290 | passwd_buf (320)           |
-------------------------------------------
```

Néanmoins, l'on peut remarquer la variable `cmp_result` a été optimisé et
n'est jamais stocké sur la stack, ceci peut être confirmé en regardant le code
desassemblé dans GDB.

```text
-------------------------------------------
| $ebp+4     | Return address (4)         |
-------------------------------------------
| $ebp       | Saved frame pointer (4)    |
-------------------------------------------
| $ebp-4     | passwd_buf ptr (4)         |
-------------------------------------------
| $ebp-8     | rand_buf ptr (4)           |
-------------------------------------------
| $ebp-12    | callback function ptr (4)  |
-------------------------------------------
| $ebp-0x14c | rand_buf (320)             |
-------------------------------------------
| $ebp-0x28c | passwd_buf (320)           |
-------------------------------------------
```

### Question 3.4

> Quel est le flag obtenu ?

`5ay5x7N2AohfNH8pTGgD`

## Buffer overflows - niveau 2

### Question 4.1

> En quoi ces protections empêchent-elles une attaque de type stack smashing

À l'aide de `checksec`, on peut voir que le binaire est compilé avec un stack
canary. Cette protection sert à vérifier que la stack lors de l'exécution d'un
binaire n'a pas été corrompue.

### Question 4.2

> Dessiner la stackframe de la fonction `secure_login`

```text
-------------------------------------------
| $ebp+4     | Return address (4)         |
-------------------------------------------
| $ebp       | Saved frame pointer (4)    |
-------------------------------------------
| $ebp-8     | - unused - (8)             |
-------------------------------------------
| $ebp-12    | canary (8)                 |
-------------------------------------------
| $ebp-16    | manual canary (4)          |
-------------------------------------------
| $ebp-516   | username                   |
-------------------------------------------
| $ebp-1020  | password                   |
-------------------------------------------
```

The manual canary is compared by the `win` function.

### Question 4.3

> Dans ce cas, que veut-on remplacer par l'adresse de la fonction `win` ?
> Quelle vulnérabilite permet de le faire ?

L'adresse de retour. On peut la remplacer à l'aide d'un buffer overflow
si on arrive à leaker la valeur du canary.

### Question 4.4

> Quelle autre valeur que l'adresse de la fonction `win` doit-on connaitre afin
> d'effectuer l'attaque ?

la valeur du canary.

> Quelle(s) vulnérabilite(s) permet(tent) d'obtenir cette valeur sans avoir
> besoin d'avoir recours à un debugger ?

### Question 4.5

> Si vous n'avez pas anticipé sur cette question, vous ne devriez pas avoir
> réussi à obtenir votre flag ? Pourquoi ?

Il existe un canary fantôme qui est set par la fonction `secure_login` juste
avant quitter la frame.

En explorant le code desassemblé, on peut déterminer sa valeur: `0xdeadbeef`.

### Question 4.6

> Quel est le flag obtenu ?

< Pas completé >

## Format strings - niveau 1

< Exercise pas résolu >

## Format strings - niveau 2

< Exercise pas résolu >
