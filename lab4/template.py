#!/usr/bin/env python
# encoding: utf-8

from pwn import *
import argparse

parser = argparse.ArgumentParser("Template d'exploitation SLO chal 1")
parser.add_argument('--file', '-f', type=str, default="./chal1", help="name of the binary to exploit")
parser.add_argument('--host', type=str, default='10.192.72.221', help='romete host')
parser.add_argument('--port', '-p', type=int, help='Port remote')
parser.add_argument('--remote', '-r', default=False, action='store_true', help='Enable remote connection')
parser.add_argument('--debug', '-d', default=False, action='store_true', help='Set loglevel to debug')
parser.add_argument('--gdb', '-g', default=False, action='store_true', help='Start gdb')

args = parser.parse_args()

if args.debug:
    context.log_level = 'DEBUG'

if args.remote:
    if args.port is None:
        log.warn('Port required for remote execution')
        sys.exit(-1)
    r = remote(args.host, args.port)
else:
    r = process(args.file)
    if args.gdb:
        gdb.attach(r, 'c')


# TODO: Student get win addr
winaddr = 0x00
log.info('Win address 0x{:08x}'.format(winaddr))

# put your payload here you might use p32(winaddr)
# Notice the "b" before the payload, it's used to define bytes instated of str
payload = b''
r.sendlineafter(b': ', payload)

r.interactive()
r.close()
