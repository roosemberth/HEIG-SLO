{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs";

  outputs = { self, nixpkgs }: let
    systems = [ "x86_64-linux" "i686-linux" ];
    forAllSystems = f: nixpkgs.lib.genAttrs systems (system: f (import nixpkgs {
      inherit system; overlays = [ self.overlay ];
      config.android_sdk.accept_license = true;
    }));
  in {
    overlay = final: prev: with final; { };

    devShell = forAllSystems (pkgs: with pkgs; stdenv.mkDerivation {
      name = "SLO-env";
      nativeBuildInputs = [
        ghidra-bin radare2-cutter
      ];
      allowSubstitutes = false;
    });

    # Export all packages from the overlay attribute.
    packages = forAllSystems (pkgs: with pkgs.lib;
      getAttrs (attrNames (self.overlay {} {})) pkgs
    );
  };
}
