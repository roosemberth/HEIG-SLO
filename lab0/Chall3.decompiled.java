/*
 * Decompiled with CFR 0.150.
 */
import java.util.Arrays;
import java.util.Scanner;

public class Chall3 {
    public static void main(String[] arrstring) {
        int[] otp = new int[]{86, 156, 85, 11, 29, 135, 204, 77, 172, 242, 83, 69, 182, 210, 161, 2, 240, 0, 87, 112, 52, 243};
        int[] target = new int[]{4, 173, 39, 56, 126, 180, 191, 122, 206, 194, 61, 53, 134, 167, 211, 110, 196, 115, 99, 30, 3, 192};
        int[] dst = new int[22];
        Scanner scanner = new Scanner(System.in);
        System.out.println("Portail TOP SECRET de la Conf\u00e9d\u00e9ration H\u00e9lv\u00e9tique\n=================================================\nEntrez votre mot de passe pour acc\u00e9der aux informations confidentielles: ");
        String attempt = scanner.nextLine();
        scanner.close();
        for (int i = 0; i < dst.length && i < attempt.length(); ++i) {
            dst[i] = (char)(attempt.charAt(i) ^ otp[i % otp.length]);
        }
        if (Arrays.toString(dst).equals(Arrays.toString(target))) {
            System.out.println("F\u00e9licitations! Vous \u00eates connect\u00e9 sur le portail TOP SECRET de la Conf\u00e9d\u00e9ration H\u00e9lv\u00e9tique.");
        } else {
            System.out.println("Le mot de passe est incorrect.");
        }
    }
}
