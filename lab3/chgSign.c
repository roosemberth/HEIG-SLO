#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include <klee/klee.h>

int chg_sign(int x, int y) {
  if (x > y) {
    assert(y - x > 0);
    return y - x;
  }
  return x - y;
}

int main() {
  int x, y;

  klee_make_symbolic(&x, sizeof x, "x");
  klee_make_symbolic(&y, sizeof y, "y");

  chg_sign(x, y);
  return 0;
}
