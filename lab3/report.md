---
title: "Lab 3 SLO: Analyse concolique"
lang: fr
author:
  - Roosembert Palacios
  - Florian Gazzetta
geometry: margin=2cm
---

## Analyser une Fonction simple

### Question 4.1

> Que fait l'option `-only-output-states-covering-new`

Instructs klee to only record one test per state reached.
Klee generates may test paths reaching different code sections, these code
sections generate states as a function of what state they were reached from.
This option restricts the generated tests to only those that reach states
previously unreached by previous tests.

### Question 4.2

> Que contiennent les fichier en `.ptr.err`. Comment les ouvrez-vous ?

Contains invalid memory access made by the project within the specified test.
It's contents are plain text, so they can be accessed with any text editor.

> Que contiennent les fichier en `.ktest`. Comment les lisez-vous ?

Contains details on a test execution, including the state of objects.

Can be displayed using `ktest-tool`. e.g.
`ktest-tool 8.2-14.regexp/test000001.ktest`.

### Question 4.3

> Que fait la fonction fournie ?

Calculates the difference between two numbers and returns its negative absolute
value.

> Quel bug a été trouvé par Klee ?

An integer overflow due to the unbalanced encoding of positive and negative
numbers.

## Labyrinthe

### Question 5.1

> Listez tous les chemins gagnants trouvés par KLEE.
> Expliquez pourquoi chacun de ces chemins est gagnant en vous référant au code.

```console
$ echo maze/*.err(#q:s/'#%(#b)(*)\/*(test*).*.*'/'${match[1]}-${match[2]}.ktest'/) \
  | xargs -n1 make | egrep 'object 1: (text|hex)'
object 1: hex : 0x73737373646464646464777777776464ffffffffffffffffffffff00
object 1: text: ssssddddddwwwwdd............
object 1: hex : 0x73737373646464646464646477777777ffffffffffffffffffffff00
object 1: text: ssssddddddddwwww............
object 1: hex : 0x7373737364646464777761617777646464646464ffffffffffffff00
object 1: text: ssssddddwwaawwdddddd........
object 1: hex : 0x73737373646464647777616177776464646473737373646477777777
object 1: text: ssssddddwwaawwddddssssddwwww
```

Most of these strings are suffixed with 'FF', this is accepted since the
program terminates as soon as the target is detected.

The first string is not a valid solution but is accepted by the program since
the bounds checking code has extraneous exceptions for the walls at positions
(8, 1) and (6, 5).
This exceptions may be a form of a backdoor.

```C
if (maze[y][x] != ' ' && !(((y == 5 || (x == 8 && y == 1)) &&
                            maze[y][x] == '|' && x > 0 && x < W))) {
  // snip... movement is illegal.
}
```

The second string uses the backdoor at position (6, 5), just like the previous
string.

The third string exploits the backdoor at position (8, 1), just like the
previous two strings.

The forth string is finally the expected solution of the maze.

## Keygen avec KLEE

### Quetion 6.1

> À quoi servent les différentes options passées à KLEE ?

From [KLEE's documentation][klee-docs]:

[klee-docs]: https://klee.github.io/docs/options/

- `--optimize` \
  _Optimizes the code before execution by running various compiler optimization
  passes._

- `--libc=uclibc` \
  From the cli `--help` message: _Link in uclibc (adapted for KLEE)_

- `--posix-runtime` \
  From the cli `--help` message: _Link with POSIX runtime._ This applows passing
  options such as `--sym-arg` to replace by a symbolic argument with the
  specified length.

- `--sym-arg 100` \
  Length of the symbolic argument.

### Question 6.2

> Quel est le mot de passe permettant de lancer le programme ?

`Expeliarmus`.

## Algorithmes de Tri

### Question 7.1

> Que fait la fonction `test()` du fichier `sort.c` ?

Checks that the arrays passed to functions `insertion_sort` and `bubble_sort`
equal after these functions run.

### Question 7.2

> Quel problème rencontrez-vous [lorsque vous essayez de lancer KLEE sur cet
> example] ?

There's an error during execution:

```console
klee -output-dir=sort --libc=uclibc --posix-runtime -emit-all-errors sort.bc
<...>
KLEE: ERROR: sort.c:52: external call with symbolic argument: printf
<...>
```

### Question 7.3

> Quel est le bug dans le code ? Donnez un patch le corrigeant.

Bubble sort would unconditionally exit after the first iteration.

```patch
diff --git a/lab3/sort.c b/lab3/sort.c
index fd63ce5..ec58825 100644
--- a/lab3/sort.c
+++ b/lab3/sort.c
@@ -31,7 +31,8 @@ void bubble_sort(int *array, unsigned nelem) {
       }
     }
 
-    break;
+    if (done != 0)
+      break;
   }
 }
 
```

## Comparaison avec le Fuzzer

### Question 8

> Que pouvez-vous dire sur les performances de KLEE sur cet exemple par rapport
> à afl ? Pourquoi avons-nous de telles différences ? Justifiez votre réponse.

The input generation in AFL has less information that the one in KLEE: For
instance, while KLEE can run structural analysis on the source to generate
inputs that hit all code paths in the source, AFL can, at best generate inputs
that hit every execution path at an instruction level.

Moreover, the symbolic execution as promoted by KLEE can be preemptively
terminated if KLEE has already explored a given state, whereas AFL requires
a full execution of the binary to decide.
In general, afl has an heuristic approach where it discovers a path that may
increase the chances of crashing, where as KLEE can generate a test plan that
may discard test cases whose state has been already explored by a previous test.
